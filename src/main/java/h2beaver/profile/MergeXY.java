package h2beaver.profile;

import h2beaver.profile.jackson.Util;
import org.geojson.Feature;
import org.geojson.FeatureCollection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class MergeXY {
    public static void main(String[] args) throws IOException {
        FeatureCollection root = new FeatureCollection();
        root.setCrs(GeoJsonHelper.getDefaultCrs());

        File inputFolder = Paths.get(args[0]).toFile();
        File[] files = inputFolder.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                System.err.print("Processing: ");
                System.err.print(files[i].getCanonicalPath());
                System.err.print("...");
                Feature p = Util.loadProfileGeojsonFile(files[i]);
                root.add(p);
                System.err.println("Done");
            }
        }

        if (args.length > 1) {
            File output = Paths.get(args[1]).toFile();
            System.err.println("Output to " + output.getCanonicalPath());
            Util.getObjectMapper().writerWithDefaultPrettyPrinter().writeValue(output, root);
        } else {
            Util.getObjectMapper().writerWithDefaultPrettyPrinter().writeValue(System.out, root);
        }
    }
}