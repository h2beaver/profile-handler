package h2beaver.profile.pojo;

import java.util.Arrays;

public class ProfileData {

    private double[][] profile = {};

    public ProfileData() {
    }

    public ProfileData(double[][] profile) {
        this.setProfile(profile);
    }


    private double[][] copy(double[][] in) {
        if (in == null) return null;
        double[][] out = new double[in.length][];
        for (int i = 0; i < in.length; i++) {
            if (in[i] == null) {
                out[i] = null;
            } else {
                out[i] = Arrays.copyOf(in[i], in[i].length);
            }
        }
        return out;
    }

    public double[][] getProfile() {
        return copy(this.profile);
    }

    public void setProfile(double[][] profile) {
        this.profile = copy(profile);
    }
}
