package h2beaver.profile.pojo;

public class UploadProfile {
    public String no;
    public Point left = new Point("Left");
    public Point right = new Point("Right");
    public Point streamReference = new Point("Stream");
    public double mainChannelDistance;
    public double elevation;
    public String note;
    public double[][] table;

    public static class Point {
        public String type;
        public double x;
        public double y;

        public Point() {
        }
        private Point(String type) {
            this.type = type;
        }
    }
}
