package h2beaver.profile.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.geojson.Feature;


@JsonTypeName("Feature")
public class ProfileFeature extends Feature {
    @JsonUnwrapped
    @JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NONE)
    public final ProfileData profile;

    public ProfileFeature(Feature feature, ProfileData profile) {
        this.profile = profile;
        super.setGeometry(feature.getGeometry());
        super.setProperties(feature.getProperties());
        super.setId(feature.getId());
    }
}
