package h2beaver.profile;

import h2beaver.profile.jackson.Util;
import h2beaver.profile.pojo.ProfileData;
import h2beaver.profile.pojo.ProfileFeature;
import h2beaver.profile.pojo.UploadProfile;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LngLatAlt;
import org.geojson.Point;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class MergeUpload {
    public static Feature convert(UploadProfile profile) {
        Feature feature = new Feature();
        Point point = new Point();
        LngLatAlt coordinate = new LngLatAlt();
        coordinate.setLongitude(profile.streamReference.x);
        coordinate.setLatitude(profile.streamReference.y);
        point.setCoordinates(coordinate);
        feature.setGeometry(point);
        feature.setId(profile.no);
        feature.setProperty("left", profile.left);
        feature.setProperty("right", profile.right);
        feature.setProperty("elevation", profile.elevation);
        feature.setProperty("mainChannelDistance", profile.mainChannelDistance);
        feature.setProperty("note", profile.note);

        return new ProfileFeature(feature, new ProfileData(profile.table));
    }

    public static void main(String[] args) throws IOException {
        FeatureCollection root = new FeatureCollection();
        root.setCrs(GeoJsonHelper.getDefaultCrs());

        File inputFolder = Paths.get(args[0]).toFile();
        File[] files = inputFolder.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if ("metadata.json".equals(files[i].getName())) {
                  System.err.println("Skip metadata.json..");
                  continue;
                }
                System.err.print("Processing: ");
                System.err.print(files[i].getCanonicalPath());
                System.err.print("...");
                UploadProfile p = Util.loadUploadedFile(files[i]);
                root.add(convert(p));
                System.err.println("Done");
            }
        }

        if (args.length > 1) {
            File output = Paths.get(args[1]).toFile();
            System.err.println("Output to " + output.getCanonicalPath());
            Util.getObjectMapper().writerWithDefaultPrettyPrinter().writeValue(output, root);
        } else {
            Util.getObjectMapper().writerWithDefaultPrettyPrinter().writeValue(System.out, root);
        }
    }
}
