package h2beaver.profile;

import org.geojson.Crs;
import org.geojson.jackson.CrsType;

import java.util.HashMap;
import java.util.Map;

public class GeoJsonHelper {
    private static Map<String, Object> crsName = new HashMap<>();
    private static Crs defaultCrs = new Crs();

    static {
        crsName.put("name", "urn:ogc:def:crs:EPSG::3826");
        defaultCrs.setType(CrsType.name);
        defaultCrs.setProperties(crsName);
    }
    public static Crs getDefaultCrs() {
        return defaultCrs;
    }
}
