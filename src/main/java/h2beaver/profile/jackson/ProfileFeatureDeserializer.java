package h2beaver.profile.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import h2beaver.profile.pojo.ProfileData;
import h2beaver.profile.pojo.ProfileFeature;
import org.geojson.Feature;
import org.geojson.GeoJsonObject;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class ProfileFeatureDeserializer extends JsonDeserializer<Feature> {

    private static TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {
    };

    public ProfileFeatureDeserializer() {
        super();
    }

    @Override
    public Feature deserialize(JsonParser parser, DeserializationContext context)
            throws IOException, JsonProcessingException {

        ObjectCodec codec = parser.getCodec();
        JsonNode node = codec.readTree(parser);
        Feature feature = new Feature();
        feature.setGeometry(codec.treeToValue(node.get("geometry"), GeoJsonObject.class));
        feature.setId(Optional.ofNullable(node.get("id")).map(n -> n.asText()).orElse(null));
        feature.setProperties(codec.readValue(codec.treeAsTokens(node.get("properties")), typeRef));

        if (node.has("profile")) {
            return new ProfileFeature(feature, codec.treeToValue(node, ProfileData.class));
        }
        return feature;
    }
}
