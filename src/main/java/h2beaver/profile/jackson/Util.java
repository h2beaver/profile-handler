package h2beaver.profile.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import h2beaver.profile.pojo.UploadProfile;
import org.geojson.Feature;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Util {
    private static ObjectMapper objectMapper = initObjectMapper();
    private static ObjectMapper initObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper()
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS);
        SimpleModule customized = new SimpleModule("ProfileFeature");
        customized.addDeserializer(Feature.class, new ProfileFeatureDeserializer());
        objectMapper.registerModule(customized);
        return objectMapper;
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static Feature loadProfileGeojsonFile(File f) throws IOException {
        Feature p = objectMapper.readValue(new FileInputStream(f), Feature.class);
        return p;
    }

    public static UploadProfile loadUploadedFile(File f) throws IOException {
        UploadProfile p = getObjectMapper().readValue(new FileInputStream(f), UploadProfile.class);
        return p;
    }
}
