const fs = require('fs');
const SPACE = "\u3000";

function parseKVPair(l, check) {
  let checkl = check.toLowerCase();
  // split with consider space inside quotations
  let e = [];
  let inside = 0;
  for (let i = 0; i < l.length; i++) {
    if (l[i] === "'") {
      inside = 1 - inside;
      e.push(l[i]);
    } else if (inside && l[i] === ' ') {
      e.push(SPACE);
    } else {
      e.push(l[i]);
    }
  }
  let parts = e.join('').split(' ').map(p=>p.trim()).filter(p=>p.length);
  let data = {};
  let key;
  let values = [];
  if (parts.shift() === check ) {
    for (let p = parts.shift(); p; p = parts.shift()) {
      if (p && p === checkl) {
        // do nothing if it's end tag
      } else if (/^[a-z][a-z0-9]$/.test(p)) {
        if (key && values.length === 1) {
          data[key] = values[0]; 
        } else if (key) {
          data[key] = values; 
        }
        key = p;
        values = [];
      } else if (p && p[0] === "'") {
        values.push(p.replace(/'/g,"").replace(/\u3000/g," "));
      } else if (p) {
        values.push(Number.parseFloat(p));
      }
    }
    if (key && values.length === 1) {
      data[key] = values[0]; 
    } else if (key) {
      data[key] = values; 
    }
  }
  return data;
}

function loadNETWORKCRSN() {
  let networkCR= fs.readFileSync('./NETWORK.CR');
  let lines = networkCR.toString().split("\n");
  return lines.map(l=>parseKVPair(l,'CRSN')).reduce((a,c) => {
    if (c.id) a[c.id] = c;
    return a; 
  },{});
}

function loadCRSN() {
  let profiledat = fs.readFileSync('./PROFILE.DAT');
  let lines = profiledat.toString().split("\n");
  return lines.map(l=>parseKVPair(l,'CRSN')).filter(l=>Object.keys(l).length>0);
}

function parseCRDS(lines) {
  let def = lines[0];
  let crdsDef = parseKVPair(def, 'CRDS');
  let tableLines = lines.slice(1).map(l=>l.trim());
  let table = [];
  for (let l=tableLines.shift();l;l=tableLines.shift()) {
    if (l ==='TBLE' || l === 'tble') {
    } else if (l.endsWith('<')) {
      let data = [];
      let parts = l.split(/\s+/);
      for (let i = 0; i < parts.length -1; i++) {
        data.push(Number.parseFloat(parts[i]));
      }
      table.push(data);
    }
  }
  return {
    ...crdsDef,
    table
  };
}

function loadCRDS() {
  let profiledef = fs.readFileSync('./PROFILE.DEF');
  let lines = profiledef.toString().split("\n").map(l=>l.trim());
//  console.log(lines.length);
  let crds = {};
  let crds_lines = [];
  lines.forEach(l=>{
    if (l.startsWith("crds") && crds_lines[0]) {
      let d = parseCRDS(crds_lines);
      if (d && d.id) crds[d.id] = d;
      crds_lines = [];
//      console.log(`CRDS ${id} ${name}`);
    } else if (crds_lines[0] && crds_lines[0].startsWith("CRDS ")) {
      crds_lines.push(l);
    } else if (l.startsWith("CRDS ") && l.endsWith("crds")) {
      let d = parseCRDS([l]);
//      console.log(d);
      if (d && d.id) crds[d.id] = d;
    } else if (l) {
      crds_lines[0]=l;
    } 
  });
  return crds;
}

function loadObjList() {
  let objlist= fs.readFileSync('./channel_objs_list.dat');
  let lines = objlist.toString().split("\n").map(l=>l.trim());
  let objs = {};
  lines.forEach(l=>{
    let parts = l.split(/\s+/);
    let reach = parts[0];
    let node = parts[1];
    let x = Number.parseFloat(parts[2]);
    let y = Number.parseFloat(parts[3]);
    let type = parts[4];
    let obj = objs[node] || {};
    obj[type] = {
      reach, node, x, y, type
    };
    objs[node] = obj;
  });
  return objs;
}

function toFeature(crsn) {
  return {
    "type": "Feature",
    "properties": crsn,
    "geometry": {
      "type": "Point",
      "coordinates": [crsn.node.x, crsn.node.y]
    },
    "profile": crsn.crds && crsn.crds.table
  };
}

function toGeojson(data) {
  return {
    "type": "FeatureCollection",
    "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3826" } },
    "features": data.filter(d=>(d.node && d.node.x && d.node.y )).map(toFeature)
  }
}

let networkCR = loadNETWORKCRSN();
let crsn = loadCRSN();
let crds = loadCRDS();
let channel_objs_list = loadObjList();

console.log(JSON.stringify(toGeojson(crsn.map(crsn=>{ 
  let network = networkCR[crsn.id];
  let nodes = network && channel_objs_list[network.ci];
  let node = nodes && (nodes.SBK_PROFILE || Object.values(nodes)[0]);
  return { ...crsn, ...network, node, crds: crds[crsn.di]}})
)));
