const fs = require('fs');

const geojson = require(process.argv[2]);
const outputPath = process.argv[3] || './';

geojson.features.forEach(feature=>{
  let node = feature.properties.node;
  if (node) {
    let filepath = `${outputPath}X${Math.round(node.x)}-Y${Math.round(node.y)}.json`;
    console.log(filepath);
    let str = JSON.stringify(feature, null, 2); // spacing level = 2
    fs.writeFileSync(filepath, str, {encoding: "utf8"});
  }
});

